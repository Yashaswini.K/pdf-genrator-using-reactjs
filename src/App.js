import logo from "./logo.svg";
import "./App.css";

import React from "react";
import Userdetails from "./components/Userdetails";
import { BrowserRouter,Route, Routes } from "react-router-dom";
import Register from "./components/signup/index";
import Login from "./components/Login/index";
import Nav from "./components/Nav";

function App() {
  return (
      <BrowserRouter>
    
    
      <Routes>
         <Route exact path="/" element={<Register/>}/>
         <Route exact path="/login" element={<Login/>}/> 
           <Route exact path="/user" element={<Userdetails/>}/>
            <Route exact path="/nav" element={<Nav/>}/>
           {/* <Route exact path="/link" element={<Link/>}/>
           <Route exact path="/notification" element={<Notification/>}/>
           <Route exact path="/logout" element={<Logout/>}/>  */}
         </Routes>
         </BrowserRouter>
   
  );
}

export default App;
