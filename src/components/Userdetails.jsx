import React, { useState, useEffect } from "react";
import DatePicker from "react-datepicker";
import ReactHTMLTableToExcel from "react-html-table-to-excel";
import ReactToPdf from "react-to-pdf";
import "react-datepicker/dist/react-datepicker.css";
import { parse, setISODay } from "date-fns";
import Viewpdf from "./Viewpdf";
import { jsPDF } from "jspdf";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

function Userdetails() {
  // Added
  const [userDetails, setUserDetails] = useState({
    date: new Date(),
    gender: "male",
    name: "",
    ifsc: "",
    country: "",
  });
  const [errName, setErrorName] = useState(false);
  const [errIfsc, setErrorIfsc] = useState(false);
  const [errCountry, setErrorCountry] = useState(false);
  const [userEditDetails, setUserEditDetails] = useState({}); // Edit Value
  const [presentDate, setPresentDate] = useState("");
  const [details, setDetails] = useState([]);
  const [id, setId] = useState("");

  const [ascending, setAscending] = useState({});

  const [filterInput, setFilterInPut] = useState("");
  const [filterData, setFilterData] = useState([]);

  //checkboxes
  const [checkedCountry, setCheckedCountry] = useState(false);
  const [checkedname, setCheckedName] = useState(false);
  const [checkedIFSC, setChecedIFSC] = useState(false);
  const [checkedgender, setCheckedGender] = useState(false);
  const [checkedDate, setCheckedDate] = useState(false);
  const [checkedAll, setCheckedAll] = useState(false);

  const ref = React.createRef();
  const options = {
    orientation: "landscape",
    unit: "in",
    format: [14, 10],
  };
  // Number of rows dropdown
  const [rows, setRows] = useState("5");
  const [noOfPage, setNoOfPage] = useState("1");

  const [editSatus, setEditStatus] = useState(false);

  const [viewStatus, setViewStatus] = useState(false);

  useEffect(() => {
    if (checkedAll) {
      setCheckedCountry(true);
      setChecedIFSC(true);
      setCheckedName(true);
      setCheckedGender(true);
      setCheckedDate(true);
    } else {
      if (
        checkedCountry &&
        checkedname &&
        checkedIFSC &&
        checkedgender &&
        checkedDate
      ) {
        setCheckedCountry(false);
        setChecedIFSC(false);
        setCheckedName(false);
        setCheckedGender(false);
        setCheckedDate(false);
      }
    }
  }, [checkedAll]);

  useEffect(() => {
    if (
      checkedCountry &&
      checkedname &&
      checkedIFSC &&
      checkedgender &&
      checkedDate
    ) {
      setCheckedAll(true);
    } else {
      setCheckedAll(false);
    }
  }, [checkedCountry, checkedname, checkedIFSC, checkedgender, checkedDate]);

  const onSubmit = (e) => {
    e.preventDefault();
    let error = false;
    debugger;
    if (userDetails.name === "") {
      setErrorName(true);
      error = true;
    } else {
      setErrorName(false);
    }
    if (userDetails.ifsc === "") {
      setErrorIfsc(true);
      error = true;
    } else {
      setErrorIfsc(false);
    }
    if (userDetails.country === "") {
      setErrorCountry(true);
      error = true;
    } else {
      setErrorCountry(false);
    }
    if (error === false) {
      toast.success("Record Added Successfully", {
        position: toast.POSITION.BOTTOM_CENTER,
        autoClose: 1000,
      });
      setErrorName(false);
      setErrorCountry(false);
      setErrorIfsc(false);
      console.log(userDetails, "userDetails");
      const months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
      ];
      let dd = [];
      let d = userDetails.date;
      dd[0] = d.getDate();
      dd[1] = months[d.getMonth()];
      dd[2] = d.getFullYear();
      let full = dd.join("-");

      let obj = {};
      obj = JSON.parse(JSON.stringify(userDetails));
      obj.date = full;
      setPresentDate(full);
      if (id === "") {
        setDetails([...details, obj]);
        setFilterData([...details, obj]);
      } else {
        // let arr = [...details];
        // arr[id] = { ...obj };
        // setDetails(arr);
        // setFilterData(arr);
        // setId("");
      }

      setUserDetails({
        country: "",
        date: new Date(),
        gender: "male",
        ifsc: "",
        name: "",
      });
    }
  };

  const editSave = (e) => {
    let error = false;
    debugger;
    if (userEditDetails.name === "") {
      error = true;
    }
    if (userEditDetails.ifsc === "") {
      error = true;
    }
    if (userEditDetails.country === "") {
      error = true;
    }
    if (error === false) {
      toast.success("Record Updated Successfully", {
        position: toast.POSITION.BOTTOM_CENTER,
        autoClose: 1000,
      });
      console.log(userEditDetails, "userEditDetails");
      const months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
      ];
      let dd = [];
      let d = userEditDetails.date;
      dd[0] = d.getDate();
      dd[1] = months[d.getMonth()];
      dd[2] = d.getFullYear();
      let full = dd.join(" - ");
      let obj = {};
      obj = JSON.parse(JSON.stringify(userEditDetails));
      obj.date = full;
      let arr = [...details];
      arr[id] = { ...obj };
      setDetails(arr);
      setFilterData(arr);
      setId("");
      setEditStatus(false);
    } else {
      toast.error("Name , Select Amount , Deduction Amount Field is Required", {
        position: toast.POSITION.BOTTOM_CENTER,
        autoClose: 1000,
      });
    }
  };
  const handleClear = (e) => {
    e.preventDefault();
    setUserDetails({
      country: "",
      date: new Date(),
      gender: "male",
      ifsc: "",
      name: "",
    });
    setId("");
    setErrorName(false);
    setErrorCountry(false);
    setErrorIfsc(false);
  };

  const deleteAll = (id) => {
    let arr = [...details];
    arr.splice(id, 1);

    setDetails(arr);
    setFilterData(arr);
    toast.success("Record Deleted Successfully", {
      position: toast.POSITION.BOTTOM_CENTER,
      autoClose: 1000,
    });
    setUserDetails({
      country: "",
      date: new Date(),
      gender: "",
      ifsc: "",
      name: "",
    });
  };

  const editData = (id) => {
    setId(id);
    setEditStatus(true);
    let arr = JSON.parse(JSON.stringify(details));
    let a = arr.splice(id, 1);
    a[0].date = new Date();
    setUserEditDetails(a[0]);
  };

  const viewData = (id) => {
    setViewStatus(true);

    let arr = JSON.parse(JSON.stringify(details));
    let a = arr.splice(id, 1);
    a[0].date = new Date("Dec 13, 2021");
    setUserEditDetails(a[0]);
  };

  const ascendingOder = (basedOn) => {
    setAscending({
      status: false,
      based: basedOn,
    });

    filterData.sort((a, b) => {
      let fa = a[basedOn].toLowerCase(),
        fb = b[basedOn].toLowerCase();

      if (fa < fb) {
        return -1;
      }
      if (fa > fb) {
        return 1;
      }
      return 0;
    });
  };

  const desendingOrder = (basedOn) => {
    setAscending({
      status: true,
      based: basedOn,
    });

    filterData.sort((a, b) => {
      let fa = a[basedOn].toLowerCase(),
        fb = b[basedOn].toLowerCase();

      if (fb < fa) {
        return -1;
      }
      if (fb > fa) {
        return 1;
      }
      return 0;
    });
  };

  useEffect(() => {
    let filterIns = details.filter((val) => {
      return (
        (checkedname &&
          val.name &&
          val.name.toLowerCase().includes(filterInput.toLowerCase())) ||
        (checkedIFSC &&
          val.ifsc &&
          val.ifsc.toLowerCase().includes(filterInput.toLowerCase())) ||
        (checkedDate &&
          val.date &&
          val.date.toLowerCase().includes(filterInput.toLowerCase())) ||
        (checkedgender &&
          val.gender &&
          val.gender.toLowerCase().includes(filterInput.toLowerCase())) ||
        (checkedCountry &&
          val.country &&
          val.country.toLowerCase().includes(filterInput.toLowerCase()))
      );
    });

    let arr = [];

    console.log(arr);
    setFilterData(arr);

    if (filterIns.length > 0) {
      filterIns.filter((val, index) => {
        if (index < Number(rows)) {
          return arr.push(val);
        }
      });
      setFilterData(arr);
    } else {
      details.filter((val, index) => {
        if (index < Number(rows)) {
          return arr.push(val);
        }
      });
      setFilterData(arr);
    }
  }, [filterInput]);

  const numberOfRows = (e) => {
    setRows(e.target.value);
  };

  useEffect(() => {
    setNoOfPage(1);
    let arr = [];
    details.filter((val, index) => {
      if (index < Number(rows)) {
        return arr.push(val);
      }
    });

    console.log(arr);
    setFilterData(arr);
  }, [rows, details]);

  useEffect(() => {
    // let a = parseInt(details.length) / parseInt(rows);
    let a = Math.ceil(parseInt(details.length) / parseInt(rows));

    let arr = details.slice(
      parseInt(rows) * (parseInt(noOfPage) - 1),
      parseInt(rows) * parseInt(noOfPage)
    );

    setFilterData(arr);
  }, [noOfPage, rows]);

  const generatePdf = () => {
    var doc = new jsPDF();

    var content = document.getElementById("content");
    doc.html(content, {
      callback: function (doc) {
        doc.save();
      },
    });
  };

  return (
    <div className="App">
      {/* navbar */}
      <nav
        className="navbar navbar-expand-lg navbar-dark "
        style={{ background: "#3bb19b" }}
      >
        <Link className="navbar-brand" to="/nav">
        Personal Loan
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNavAltMarkup"
          aria-controls="navbarNavAltMarkup"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div className="navbar-nav">
            <Link className="nav-item nav-link active" to="/nav">
              Dashboard <span className="sr-only">(current)</span>
            </Link>
            <Link className="nav-item nav-link active" to="/user">
              ApplicationForm
            </Link>
            <Link
              className="nav-item nav-link active"
              to="/login"
              style={{
                marginLeft: "250%",
                border: "solid",
                borderRadius: "20px",
                background: "white",
                paddingLeft: "20%",
                paddingRight: "20%",
              }}
            >
              <b style={{ color: "black" }}> Logout</b>
            </Link>
          </div>
        </div>
      </nav>
      {/* navbar ends */}
      <div class="container ">
        <div class="panel panel-default mt-5">
          <form class="panel-body">
            <div class="row">
              <div class="col-md-6">
                <input
                  type="text"
                  class="form-control"
                  id="name"
                  value={userDetails.name ? userDetails.name : ""}
                  placeholder="Enter Name"
                  name="name"
                  onChange={(e) =>
                    setUserDetails({
                      ...userDetails,
                      name: e.target.value,
                    })
                  }
                />
                {errName && (
                  <div>
                    <p style={{ color: "red", textAlign: "left" }}>
                      Please enter the username
                    </p>
                  </div>
                )}
              </div>
              <div class="col-md-6">
                {/* <input
                  type="number"
                  class="form-control"
                  placeholder="Enter the amount to deduct per month"
                  name="ifsc"
                  value={userDetails.ifsc}
                  onChange={(e) =>
                    setUserDetails({
                      ...userDetails,
                      ifsc: e.target.value,
                    })
                  }
                /> */}
                <select
                  class="form-control"
                  id="sel1"
                  value={userDetails.country}
                  onChange={(e) =>
                    setUserDetails({
                      ...userDetails,
                      country: e.target.value,
                    })
                  }
                >
                  <option value="">Select Amount for Sanction</option>
                  <option value="50000">50000</option>
                  <option value="100000">100000</option>
                  <option value="200000">200000</option>
                </select>
                {errCountry && (
                  <div>
                    <p style={{ color: "red", textAlign: "left" }}>
                      Please enter an amount for sanction
                    </p>
                  </div>
                )}
              </div>
            </div>
            <div class="row mt-3">
              <div class="col-md-6">
                <label class="pr-2">Gender</label>
                <div class="form-check-inline">
                  <label class="form-check-label" for="radio1">
                    <input
                      type="radio"
                      class="form-check-input"
                      id="radio1"
                      name="optradio"
                      value="option1"
                      checked={userDetails.gender === "male"}
                      onClick={() => {
                        setUserDetails({
                          ...userDetails,
                          gender: "male",
                        });
                      }}
                    />
                    Male
                  </label>
                </div>
                <div class="form-check-inline">
                  <label class="form-check-label" for="radio2">
                    <input
                      type="radio"
                      class="form-check-input"
                      id="radio2"
                      name="optradio"
                      value="option2"
                      checked={userDetails.gender === "female"}
                      onClick={() => {
                        setUserDetails({
                          ...userDetails,
                          gender: "female",
                        });
                      }}
                    />
                    Female
                  </label>
                </div>
              </div>
              <div class="col-md-6">
                {/* <label for="sel1">Select list (select one):</label> */}
                {/* <select
                  class="form-control"
                  id="sel1"
                  value={userDetails.country}
                  onChange={(e) =>
                    setUserDetails({
                      ...userDetails,
                      country: e.target.value,
                    })
                  }
                >
                  <option value="">Select Amount for Sanction</option>
                  <option value="50000">50000</option>
                  <option value="100000">100000</option>
                  <option value="200000">200000</option>
                </select> */}

                <input
                  type="number"
                  class="form-control"
                  placeholder="Enter the Amount to Deduct per Month"
                  name="ifsc"
                  value={userDetails.ifsc}
                  onChange={(e) =>
                    setUserDetails({
                      ...userDetails,
                      ifsc: e.target.value,
                    })
                  }
                />
                {errIfsc && (
                  <div>
                    <p style={{ color: "red", textAlign: "left" }}>
                      Please enter the amount to deduct per month
                    </p>
                  </div>
                )}
              </div>
            </div>
            <div class="row mt-3">
              <div class="col-md-2">Date of Apply : </div>
              <div class="col-md-4">
                <DatePicker
                  className="form-control"
                  selected={userDetails.date}
                  onChange={(dates) => {
                    setUserDetails({
                      ...userDetails,
                      date: dates,
                    });
                  }}
                  name="startDate"
                  dateFormat="dd/MM/yyyy"
                  readOnly
                />
              </div>
              <div class="col-md-3">
                <button
                  onClick={handleClear}
                  className="btn btn-info btn-block"
                >
                  Clear
                </button>
              </div>
              <div class="col-md-3 ">
                <button
                  type="submit"
                  class="btn  btn-info btn-block "
                  onClick={onSubmit}
                >
                 Submit
                </button>
              </div>
            </div>
          </form>
          {/* <h1> Details</h1> */}
          <br />
          <hr></hr>
           {details.length > 0 && (
            <>
            <h3 style={{textAlign:"left",margin:"2%,2%,2%"}}><b>Applicant Details</b></h3>
            <br></br>
              <div class="row">
                <div class="form-group col-md-6">
                  <div class="input-group">
                    <input
                      type="text"
                      class="form-control"
                      value={filterInput}
                      onChange={(e) => setFilterInPut(e.target.value)}
                      placeholder="Search this blog"
                    />
                    <div class="input-group-append">
                      <button class="btn btn-secondary" type="button">
                        <i class="fa fa-search"></i>
                      </button>
                    </div>
                  </div>
                  {/* <input
                    class="form-control rounded-0 py-2"
                    type="search"
                    value={filterInput}
                    onChange={(e) => setFilterInPut(e.target.value)}
                    id="example-search-input"
                    placeholder="Search Here"
                  /> */}
                </div>
                <div class="form-group col-md-6">
                  <div class="form-check form-check-inline">
                    <input
                      class="form-check-input"
                      type="checkbox"
                      id="inlineCheckbox1"
                      value="option1"
                      checked={checkedAll}
                      onChange={(e) => {
                        setCheckedAll(e.target.checked);
                      }}
                    />
                    <label class="form-check-label" for="inlineCheckbox1">
                      All
                    </label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input
                      class="form-check-input"
                      type="checkbox"
                      id="inlineCheckbox1"
                      value="option1"
                      checked={checkedname}
                      onChange={(e) => {
                        setCheckedName(e.target.checked);
                      }}
                    />
                    <label class="form-check-label" for="inlineCheckbox1">
                      Name
                    </label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input
                      class="form-check-input"
                      type="checkbox"
                      id="inlineCheckbox2"
                      value="option2"
                      checked={checkedIFSC}
                      onChange={(e) => setChecedIFSC(e.target.checked)}
                    />
                    <label class="form-check-label" for="inlineCheckbox2">
                      Deducted Amount
                    </label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input
                      class="form-check-input"
                      type="checkbox"
                      id="inlineCheckbox3"
                      value="option3"
                      checked={checkedgender}
                      onChange={(e) => setCheckedGender(e.target.checked)}
                      // disable
                    />
                    <label class="form-check-label" for="inlineCheckbox3">
                      Gender
                    </label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input
                      class="form-check-input"
                      type="checkbox"
                      id="inlineCheckbox3"
                      value="option3"
                      checked={checkedDate}
                      onChange={(e) => setCheckedDate(e.target.checked)}
                      // disabled
                    />
                    <label class="form-check-label" for="inlineCheckbox3">
                      Date
                    </label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input
                      class="form-check-input"
                      type="checkbox"
                      id="inlineCheckbox3"
                      value="option3"
                      checked={checkedCountry}
                      onChange={(e) => setCheckedCountry(e.target.checked)}

                      // disabled
                    />
                    <label class="form-check-label" for="inlineCheckbox3">
                      Select Amount
                    </label>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <ReactHTMLTableToExcel
                    id="test-table-xls-button"
                    className="download-table-xls-button btn btn-info btn-block"
                    table="table-to-xls"
                    filename="tablexls"
                    sheet="tablexls"
                    buttonText="Download as XLS"
                  />
                </div>
                <div class="col-md-6">
                  <ReactToPdf
                    targetRef={ref}
                    filename="div-blue.pdf"
                    options={options}
                    x={0}
                    y={1}
                    scale={0.8}
                  >
                    {({ toPdf }) => (
                      <button
                        className="btn btn-info btn-block"
                        onClick={toPdf}
                      >
                        Generate PDF
                      </button>
                    )}
                  </ReactToPdf>
                </div>
              </div>
              <table className="table" id="table-to-xls" ref={ref}>
                <thead>
                  <tr>
                    <th>Sl.Num</th>
                    <th>
                      Name
                      {ascending.status && ascending.based === "name" ? (
                        <i
                          class="fas fa-sort-amount-up-alt"
                          onClick={() => ascendingOder("name")}
                        ></i>
                      ) : (
                        <i
                          class="fas fa-sort-amount-down"
                          onClick={() => desendingOrder("name")}
                        ></i>
                      )}
                    </th>
                    <th>
                      Gender{" "}
                      {ascending.status && ascending.based === "gender" ? (
                        <i
                          class="fas fa-sort-amount-up-alt"
                          onClick={() => ascendingOder("gender")}
                        ></i>
                      ) : (
                        <i
                          class="fas fa-sort-amount-down"
                          onClick={() => desendingOrder("gender")}
                        ></i>
                      )}
                    </th>

                    <th>
                      Select Amount
                      {ascending.status && ascending.based === "country" ? (
                        <i
                          class="fas fa-sort-amount-up-alt"
                          onClick={() => ascendingOder("country")}
                        ></i>
                      ) : (
                        <i
                          class="fas fa-sort-amount-down"
                          onClick={() => desendingOrder("country")}
                        ></i>
                      )}
                    </th>

                    <th>
                      Deducted Amount
                      {ascending.status && ascending.based === "ifsc" ? (
                        <i
                          class="fas fa-sort-amount-up-alt"
                          onClick={() => ascendingOder("ifsc")}
                        ></i>
                      ) : (
                        <i
                          class="fas fa-sort-amount-down"
                          onClick={() => desendingOrder("ifsc")}
                        ></i>
                      )}
                    </th>
                    <th>
                      Applied Date
                      {ascending.status && ascending.based === "date" ? (
                        <i
                          class="fas fa-sort-amount-up-alt"
                          onClick={() => ascendingOder("date")}
                        ></i>
                      ) : (
                        <i
                          class="fas fa-sort-amount-down"
                          onClick={() => desendingOrder("date")}
                        ></i>
                      )}
                    </th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {filterData.map((val, index) => {
                    return (
                      <tr>
                        <td>{index + 1}</td>
                        <td>
                          {editSatus && id === index ? (
                            <input
                              value={userEditDetails.name}
                              onChange={(e) => {
                                setUserEditDetails({
                                  ...userEditDetails,
                                  name: e.target.value,
                                });
                              }}
                            />
                          ) : (
                            val.name
                          )}
                        </td>{" "}
                        <td>
                          {editSatus && id === index ? (
                            <>
                              <div class="form-check-inline">
                                <label class="form-check-label" for="radio1">
                                  <input
                                    type="radio"
                                    class="form-check-input ml-0"
                                    id="radio1"
                                    name="optradio"
                                    value="option1"
                                    checked={userEditDetails.gender === "male"}
                                    onClick={() => {
                                      setUserEditDetails({
                                        ...userEditDetails,
                                        gender: "male",
                                      });
                                    }}
                                  />
                                  Male
                                </label>
                              </div>
                              <div class="form-check-inline">
                                <label class="form-check-label" for="radio2">
                                  <input
                                    type="radio"
                                    class="form-check-input ml-3"
                                    id="radio2"
                                    name="optradio"
                                    value="option2"
                                    checked={
                                      userEditDetails.gender === "female"
                                    }
                                    onClick={() => {
                                      setUserEditDetails({
                                        ...userEditDetails,
                                        gender: "female",
                                      });
                                    }}
                                  />
                                  Female
                                </label>
                              </div>
                            </>
                          ) : (
                            val.gender
                          )}
                        </td>
                        {/* <td>{val.date}</td> */}
                        <td>
                          {editSatus && id === index ? (
                            <select
                              class="form-control"
                              id="sel1"
                              value={userEditDetails.country}
                              onChange={(e) =>
                                setUserEditDetails({
                                  ...userEditDetails,
                                  country: e.target.value,
                                })
                              }
                            >
                              <option value="">
                                Select Amount for Sanction
                              </option>
                              <option value="50000">50000</option>
                              <option value="100000">100000</option>
                              <option value="200000">200000</option>
                            </select>
                          ) : (
                            val.country
                          )}
                        </td>
                        <td>
                          {editSatus && id === index ? (
                            <input
                              value={userEditDetails.ifsc}
                              onChange={(e) => {
                                setUserEditDetails({
                                  ...userEditDetails,
                                  ifsc: e.target.value,
                                });
                              }}
                            />
                          ) : (
                            val.ifsc
                          )}
                        </td>
                        <td>
                          {editSatus && id === index ? (
                            <DatePicker
                              className="form-control"
                              selected={userEditDetails.date}
                              onChange={(dates) => {
                                setUserEditDetails({
                                  ...userEditDetails,
                                  date: dates,
                                });
                              }}
                              name="startDate"
                              dateFormat="dd/MM/yyyy"
                              readOnly
                            />
                          ) : (
                            val.date
                          )}
                        </td>
                        <td>
                          {editSatus && id === index ? (
                            <i
                              class="fas fa-save mr-3"
                              onClick={() => editSave(index)}
                            ></i>
                          ) : (
                            <i
                              class="fas fa-pencil-alt mr-3"
                              onClick={() => editData(index)}
                            ></i>
                          )}

                          <i
                            class="fas fa-trash-alt"
                            onClick={() => {
                              deleteAll(index);
                            }}
                          ></i>
                          <i
                            class="fa fa-eye ml-3"
                            aria-hidden="true"
                            data-toggle="modal"
                            data-target="#myModal"
                            onClick={() => viewData(index)}
                          ></i>
                          {/* <button type="button">View</button> */}
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>

              <div class="row">
                <div class="col-md-3">
                  Total Number of Records : {details.length}
                </div>
                <div class="col-md-1">
                  <select
                    class="form-control"
                    id="sel1"
                    value={rows}
                    onChange={(e) => numberOfRows(e)}
                  >
                    <option value="">Select rows</option>
                    <option value="5">5</option>
                    <option value="10">10</option>
                    <option value="15">15</option>
                  </select>
                </div>
                <div class="col-md-5"></div>
                <div class="col-md-1 ml-5">
                  <nav aria-label="Page navigation example">
                    <ul class="pagination">
                      <li class="page-item">
                        <a
                          class="page-link"
                          href="#"
                          aria-label="Previous"
                          onClick={() => {
                            {
                              let num =
                                noOfPage > 1
                                  ? parseInt(noOfPage) - 1
                                  : noOfPage;
                              setNoOfPage(num);
                            }
                          }}
                        >
                          <span aria-hidden="true">&laquo;</span>
                          <span class="sr-only">Previous</span>
                        </a>
                      </li>
                      <li class="page-item">
                        <a class="page-link" href="#">
                          {noOfPage}
                        </a>
                      </li>

                      <li class="page-item">
                        <a
                          class="page-link"
                          href="#"
                          aria-label="Next"
                          onClick={() => {
                            {
                              let num =
                                noOfPage <
                                Math.ceil(
                                  parseInt(details.length) / parseInt(rows)
                                )
                                  ? parseInt(noOfPage) + 1
                                  : noOfPage;
                              setNoOfPage(num);
                            }
                          }}
                        >
                          <span aria-hidden="true">&raquo;</span>
                          <span class="sr-only">Next</span>
                        </a>
                      </li>
                    </ul>
                  </nav>
                </div>
              </div>
            </>
          )}
        </div>
      </div>

      <div class="modal" id="myModal">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header ">
              <h4 class="modal-title">Applicant Letter</h4>
              <button type="button" class="close" data-dismiss="modal">
                &times;
              </button>
            </div>

            <div class="modal-body " id="content">
              <pre style={{ fontSize: "8px" }}> Date : {presentDate}</pre>
              <pre style={{ fontSize: "8px" }}>Hi Sir </pre>
              <br />
              <pre style={{ fontSize: "8px" }}>
                {" "}
                I am most respectfully writing this
              </pre>
              <pre style={{ fontSize: "8px" }}>
                letter to request you for sanctioning{" "}
              </pre>
              <pre style={{ fontSize: "8px" }}>
                personal loan in my name {userEditDetails.name}{" "}
              </pre>
              <pre style={{ fontSize: "8px" }}>
                amounting {userEditDetails.country}. As per rules, the
              </pre>
              <pre style={{ fontSize: "8px" }}>
                repayment of the loan will be done by{" "}
              </pre>
              <pre style={{ fontSize: "8px" }}>
                deducting {userEditDetails.ifsc} from my salary .{" "}
              </pre>

              <pre style={{ fontSize: "8px" }}> Thanking you</pre>
            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">
                Close
              </button>
              <button
                type="button"
                class="btn btn-info"
                onClick={generatePdf}
                type="primary"
              >
                Generate PDF
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Userdetails;
