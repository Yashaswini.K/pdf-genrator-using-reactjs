import React from "react";
import { Link } from "react-router-dom";
import { Navigation, Pagination, Scrollbar, A11y } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import  hero from './hero-img.png'

export default function nav() {
  return (
    <div>
      {/* nav start */}
      <nav
        className="navbar navbar-expand-lg navbar-dark"
        style={{ background: "#3bb19b" }}
      >
        <Link className="navbar-brand" to="/nav">
          Personal Loan
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNavAltMarkup"
          aria-controls="navbarNavAltMarkup"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div className="navbar-nav">
            <Link className="nav-item nav-link active" to="/nav">
              Dashboard <span className="sr-only">(current)</span>
            </Link>
            <Link className="nav-item nav-link active" to="/user">
              ApplicationForm
            </Link>
            <Link
              className="nav-item nav-link active"
              to="/login"
              style={{marginLeft:"230%",border:"solid",borderRadius:"20px",background:'white',paddingLeft:"20%",paddingRight:"20%" }}
            >
             <b style={{color:"black"}}> Logout</b>
            </Link>
          </div>
        </div>
      </nav>
      {/* nav ends */}
      {/* carousel start */}
      <div
        id="carouselExampleIndicators"
        class="carousel slide"
        data-ride="carousel"
      >
        <ol class="carousel-indicators">
          <li
            data-target="#carouselExampleIndicators"
            data-slide-to="0"
            class="active"
          ></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img
              width="100%"
              height="300px"
              src="https://cdn.pixabay.com/photo/2018/08/17/14/26/piggy-3612928_1280.jpg"
              alt="First slide"
            />
          </div>
          <div class="carousel-item">
            <img
              width="100%"
              height="300px"
              src="https://cdn.pixabay.com/photo/2020/02/18/08/35/finance-4858797__340.jpg"
              alt="Second slide"
            />
          </div>
          <div class="carousel-item">
            <img
              width="100%"
              height="300px"
              src="https://cdn.pixabay.com/photo/2018/01/17/04/14/stock-exchange-3087396__480.jpg"
              alt="Third slide"
            />
          </div>
        </div>
        <a
          class="carousel-control-prev"
          href="#carouselExampleIndicators"
          role="button"
          data-slide="prev"
        >
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a
          class="carousel-control-next"
          href="#carouselExampleIndicators"
          role="button"
          data-slide="next"
        >
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      {/* carousel ends */}

      {/* center div */}
    
      <div className="mt-3" >
        <div className="row">
          <div className="col-md-6" style={{marginBottom: "-20%"}} >
            <img style={{borderRadius:"2%"}} height="55%" width="90%"src={hero} />
          </div>
          <div className="col-md-6 mt-5">
            <ul>
          <h4><b> 5 Government Business Loan Schemes in India 2022</b></h4>
              <li style={{fontSize:"19px",padding:"1%"}}>MSME Loan in 59 Minutes</li>
              <li style={{fontSize:"19px",padding:"1%"}}>Pradhan Mantri MUDRA Yojana (PMMY)</li>
              <li style={{fontSize:"19px",padding:"1%"}}>Credit Guarantee Fund Scheme for Micro and Small Enterprises (CGFMSE)</li>
              <li style={{fontSize:"19px",padding:"1%"}}>National Small Industries Corporation (NSIC)</li>
              <li style={{fontSize:"19px",padding:"1%"}}>Credit Linked Capital Subsidy Scheme (CLCSS)</li>
            </ul>
          </div>
        </div>
      </div>
      {/* center div ends*/}

      {/* swiper */}
      <Swiper
        // install Swiper modules
        modules={[Navigation, Pagination, Scrollbar, A11y]}
        spaceBetween={10}
        slidesPerView={4}
        navigation
        pagination={{ clickable: true }}
        scrollbar={{ draggable: true }}
        onSwiper={(swiper) => console.log(swiper)}
        onSlideChange={() => console.log("slide change")}
      >
        <SwiperSlide>
          <div class="card" style={{ width: "18rem" }}>
            <img
              src="https://media.istockphoto.com/photos/the-farmer-holds-a-money-bag-on-the-background-of-plantations-lending-picture-id1187320693?k=20&m=1187320693&s=612x612&w=0&h=hkbl2ATfUBwQuWc8BMLHYbwSc8fN5CnU1IM_QUGUvL4="
              class="card-img-top"
              alt="..."
            />
            <div class="card-body">
              {/* <h5 class="card-title">Card title</h5> */}
              <p class="card-text">
                Agriculture loans are loans that are provided to farmers to meet
                the expenses of their day-to-day or general agricultural
                requirements. These loans can be short term or long term.
              </p>
              {/* <a href="#" class="btn btn-primary">Go somewhere</a> */}
            </div>
          </div>
          {/* <img  width="100%" height="50%" src="https://cdn.pixabay.com/photo/2017/09/07/08/54/money-2724241__480.jpg"></img> */}
        </SwiperSlide>

        <SwiperSlide>
          <div class="card" style={{ width: "18rem" }}>
            <img
              src="https://media.istockphoto.com/photos/indian-gold-jewellery-with-brand-new-indian-currency-bank-notes-cash-picture-id915314912?k=20&m=915314912&s=612x612&w=0&h=KSZVCKVDdllhji07zVcqfMGmIxVbqr7rHQdxhZnevso="
              class="card-img-top"
              alt="..."
            />
            <div class="card-body">
              {/* <h5 class="card-title">Card title</h5> */}
              <p class="card-text">
                The loan against gold is a secured loan where gold is placed as
                security or collateral in return for a loan amount that
                corresponds to the per gram market value of gold on the day that
                the gold has been pledged.
              </p>
              {/* <a href="#" class="btn btn-primary">Go somewhere</a> */}
            </div>
          </div>
          {/* <img width="100%" height="50%" src="https://cdn.pixabay.com/photo/2016/04/25/23/30/house-1353389__340.jpg"></img></SwiperSlide> */}
        </SwiperSlide>

        <SwiperSlide>
          <div class="card" style={{ width: "18rem" }}>
            <img
              src="https://media.istockphoto.com/photos/coins-stack-in-columns-on-saving-book-and-car-picture-id637692094?k=20&m=637692094&s=612x612&w=0&h=FKassw0R6bQNW4xGXQ4oUpkIfSPZAfz4m79B2ZaQGKM="
              class="card-img-top"
              alt="..."
            />
            <div class="card-body">
              {/* <h5 class="card-title">Card title</h5> */}
              <p class="card-text">
                A car loan helps you to pave the path between your dream of
                owning a car and actually buying your car. Since credit reports
                are crucial for judging your eligibility towards any loan.
              </p>
              {/* <a href="#" class="btn btn-primary">Go somewhere</a> */}
            </div>
          </div>
          {/* <img width="100%" height="50%" src="https://cdn.pixabay.com/photo/2021/10/07/15/23/real-estate-6688945_1280.jpg"></img> */}
        </SwiperSlide>
        <SwiperSlide>
          <div class="card" style={{ width: "18rem" }}>
            <img
              src="https://media.istockphoto.com/photos/money-cost-saving-or-money-reserve-for-goal-and-success-in-school-picture-id1025444638?k=20&m=1025444638&s=612x612&w=0&h=mE9kl4ZWkwQ7V_ARh73dlQNVh5rjzfvDUzd5HczMgvI="
              class="card-img-top"
              alt="..."
            />
            <div class="card-body">
              {/* <h5 class="card-title">Card title</h5> */}
              <p class="card-text">
                An education loan is availed specifically to finance educational
                requirements towards school or college. Depending on the lender,
                it will cover the basic fees of the course, & other
                miscellaneous charges.
              </p>
              {/* <a href="#" class="btn btn-primary">Go somewhere</a> */}
            </div>
          </div>
          {/* <img width="100%" height="50%" src="https://cdn.pixabay.com/photo/2021/10/07/15/23/real-estate-6688945_1280.jpg"></img> */}
        </SwiperSlide>
        <SwiperSlide>
          <div class="card" style={{ width: "18rem" }}>
            <img
              src="https://media.istockphoto.com/photos/personal-loan-application-closeup-picture-id185327267?k=20&m=185327267&s=612x612&w=0&h=IhkOnp3_kAD7Ddymj7wLBKaq1fALmFuYada7Ez0rqlg="
              class="card-img-top"
              alt="..."
            />
            <div class="card-body">
              {/* <h5 class="card-title">Card title</h5> */}
              <p class="card-text">
                Most banks offer personal loans to their customers and the money
                can be used for any expense like paying a bill or purchasing a
                new television. Generally, these loans are unsecured loans.
              </p>
              {/* <a href="#" class="btn btn-primary">Go somewhere</a> */}
            </div>
          </div>
          {/* <img width="100%" height="50%" src="https://cdn.pixabay.com/photo/2021/10/07/15/23/real-estate-6688945_1280.jpg"></img> */}
        </SwiperSlide>
        <SwiperSlide>
          <div class="card" style={{ width: "18rem" }}>
            <img
              src="https://media.istockphoto.com/photos/house-mortgage-loan-buy-sell-price-real-estate-investment-money-picture-id1127490337?k=20&m=1127490337&s=612x612&w=0&h=2Es80d-mYak8LTnBOiVKbmgrrEZAB9AxtELi1SBVxhc="
              class="card-img-top"
              alt="..."
            />
            <div class="card-body">
              {/* <h5 class="card-title">Card title</h5> */}
              <p class="card-text">
                When you wish to purchase a house,It provides you the financial
                support and helps you buy the house for yourself and your loved
                ones. These loan generally come with longer tenures.
              </p>
              {/* <a href="#" class="btn btn-primary">Go somewhere</a> */}
            </div>
          </div>
          {/* <img width="100%" height="50%" src="https://cdn.pixabay.com/photo/2021/10/07/15/23/real-estate-6688945_1280.jpg"></img> */}
        </SwiperSlide>
        <SwiperSlide>
          <div class="card" style={{ width: "18rem" }}>
            <img
              src="https://media.istockphoto.com/vectors/small-business-loan-funds-businessman-carry-money-sack-in-coronavirus-vector-id1220226040?k=20&m=1220226040&s=612x612&w=0&h=FpEd7E47gOQomE8SN5tVozj6A8Rw8WAbhCKrXd1p8-k="
              class="card-img-top"
              alt="..."
            />
            <div class="card-body">
              {/* <h5 class="card-title">Card title</h5> */}
              <p class="card-text">
                Small Business Loans are loans that are provided to small scale
                and medium scale businesses to meet various business
                requirements. These loans can be used for a variety of purposes
                that help in growing the business.
              </p>
              {/* <a href="#" class="btn btn-primary">Go somewhere</a> */}
            </div>
          </div>
          {/* <img width="100%" height="50%" src="https://cdn.pixabay.com/photo/2021/10/07/15/23/real-estate-6688945_1280.jpg"></img> */}
        </SwiperSlide>
        <SwiperSlide>
          <div class="card" style={{ width: "18rem" }}>
            <img
              src="https://media.istockphoto.com/photos/people-use-credit-cards-user-data-entry-and-codes-for-electronic-picture-id1226002053?k=20&m=1226002053&s=612x612&w=0&h=_Ulgl0obXvjNh6chuQW1mmlLANyJYdw4nT3afoAcWH4="
              class="card-img-top"
              alt="..."
            />
            <div class="card-body">
              {/* <h5 class="card-title">Card title</h5> */}
              <p class="card-text">
                When you are using a credit card, you must understand that you
                will have to repay for all the purchases you make at the end of
                the billing cycle. Credit cards are accepted almost everywhere,
                even when you are travelling abroad.
              </p>
              {/* <a href="#" class="btn btn-primary">Go somewhere</a> */}
            </div>
          </div>
          {/* <img width="100%" height="50%" src="https://cdn.pixabay.com/photo/2017/05/18/11/04/key-2323278_1280.jpg"></img> */}
        </SwiperSlide>
      </Swiper>
      {/* swiper ends */}
    </div>
  );
}
