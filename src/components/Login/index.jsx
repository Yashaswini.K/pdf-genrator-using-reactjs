import styles from "./styles.module.css";
import { useState } from "react";
import React from "react";
import { Link, useNavigate } from "react-router-dom";
import {  toast } from 'react-toastify';
  import 'react-toastify/dist/ReactToastify.css';
  toast.configure();

function Login() {
  const [data, setData] = useState({
    email: "",
    password: "",
  });
  const [error, setError] = useState("");
  const navigate = useNavigate();

  const handleChange = ({ currentTarget: input }) => {
        setData({ ...data, [input.name]: input.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    debugger;
    let usersdata = JSON.parse(localStorage.getItem("details"));
    console.log(usersdata);
    console.log(data);
    if (
      usersdata.email === data.email &&
      usersdata.password === data.password
    ) {
      navigate("/nav");
      
    }else{
      toast.error('Invalid Credential', {
        position: toast.POSITION.BOTTOM_CENTER, autoClose:15000})
    }
  };

  return (
    <div className={styles.login_container}>
      <div className={styles.login_form_container}>
        <div className={styles.left}>
          <form className={styles.form_container} onSubmit={handleSubmit}>
            <h1>Login to Your Account?</h1>

            <input
              type="text"
              placeholder="Email"
              name="email"
              value={data.email}
              onChange={handleChange}
              required
              className={styles.input}
            />
            <input
              type="password"
              placeholder="Password"
              name="password"
              value={data.password}
              onChange={handleChange}
              required
              className={styles.input}
            />
            {error && <div classname={styles.error_msg}>{error}</div>}
            {/* <Link to="/nav"> */}
            <button type="submit" className={styles.green_btn}>
              Sign In
            </button>
            {/* </Link> */}
          </form>
        </div>
        <div className={styles.right}>
          <h1>New Here?</h1>
          <Link to="/">
            <button type="button" className={styles.white_btn}>
              Sign Up
            </button>
          </Link>
        </div>
      </div>
      {/* <h1>bhvh</h1> */}
    </div>
  );
}

export default Login;
